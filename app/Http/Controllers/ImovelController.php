<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Imovel;
use Validator;
use Illuminate\Pagination\Paginator; //faz com que a páginacção funcione corretamente
use Illuminate\Support\Facades\DB; //permite usar o BD table

class ImovelController extends Controller
{
    //função que faz a validação do form
    protected function validarImovel($request){
        $validator = Validator::make($request->all(), [
            "descricao" => "required",
            "logradouroEndereco"=> "required",
            "bairroEndereco" => "required",
            "numeroEndereco" => "required | numeric",
            "cepEndereco" => "required",
            "cidadeEndereco" => "required",
            "preco" => "required | numeric",
            "qtdQuartos" => "required | numeric ",
            "tipo" => "required",
            "finalidade" => "required"
        ]);
        return $validator;
    }
    
    public function index(Request $request){

        $qtd = $request['qtd'] ?: 2; //define a quantidade de itens por página
        $page = $request['page'] ?: 1;
        $buscar = $request['buscar'];
        $tipo = $request['tipo'];
    
        Paginator::currentPageResolver(function () use ($page){
            return $page;
        });

        //filtra de acordo com os parametros informados
        if($buscar){
            //$imoveis = DB::table('imoveis')->where('cidadeEndereco', '=', $buscar)->paginate($qtd);
            $imoveis = DB::table('imoveis')->where('cidadeEndereco','like','%'.$buscar.'%')->paginate($qtd);
        }else{
            if($tipo){
                $imoveis = DB::table('imoveis')->where('tipo', '=', $tipo)->paginate($qtd);
                
            }else{
                $imoveis = DB::table('imoveis')->paginate($qtd);
            }
        }
        //apends capitura na requisição todos os atributos menos page
        $imoveis = $imoveis->appends(Request::capture()->except('page'));
       
        //$imoveis = Imovel::all();
        return view('imoveis.index', compact('imoveis'));
    }
    


    public function adicionar(){

        return view('imoveis.adicionar');
    }

  

    public function salvar(Request $request){

        $validator = $this->validarImovel($request);
        if($validator->fails()){
            return redirect()->back()->withErrors($validator->errors());
        }

        //Imovel::create($request->all());
        $dados = $request->all();
        Imovel::create($dados);

        //return redirect()->route('imoveis.adicionar'); - caso queira fazer de maneira constante
        return redirect()->route('imoveis.index'); // cadastrado 1 apenas, menos produtivo
    }

    public function editar($id){
        $imovel = Imovel::find($id);
        return view('imoveis.editar',compact('imovel'));
    }

    public function atualizar(Request $request, $id){

        $validator = $this->validarImovel($request);

        if($validator->fails()){
            return redirect()->back()->withErrors($validator->errors());
        }

        //Imovel::find($id)->update($request->all());

        $imovel = Imovel::find($id);
        $dados = $request->all();
        $imovel->update($dados);

        return redirect()->route('imoveis.index');
    }

    public function deletar($id){

        Imovel::find($id)->delete();
        return redirect()->route('imoveis.index');
    }

    public function remover($id){
        $imovel = Imovel::find($id);

        return view('imoveis.remove', compact('imovel'));
    }


    public function detalhe($id){
        $imovel = imovel::find($id);
        return view('imoveis.detalhe',compact('imovel'));
    }
}
